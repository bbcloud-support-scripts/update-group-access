username      = '' # Cloud admin username
password      = '' # Cloud app password
workspace     = '' # Cloud workspace slug/id (as seen in the url)
group         = '' # Groups slug (as seen in the url)
access        = '' # Access level you wish to provide for this group (read, write or admin)