from requests import Session
from time import sleep

try:
    import env
    username  = env.username
    password  = env.password
    workspace = env.workspace
    group     = env.group
    access    = env.access

    session = Session()
    session.auth = (username, password)
    repos_url = f"https://bitbucket.org/api/internal/workspaces/{workspace}/groups/{group}/add-repositories?fields=*,values.*"
    add_url   = f"https://api.bitbucket.org/1.0/group-privileges/{workspace}/"
except [ImportError, AttributeError]:
    print('Could not locate one or more attributes within the "env.py" file. '
          'Please follow the readme to ensure that all attributes are present and try again.')
    exit()

def get_missing_repos(page=None):
    while True:
        params = {'page': page, 'pagelen': 100}
        r = session.get(repos_url, params=params)
        while r.status_code == 429:
            print("Hit the API rate limit. Sleeping for 10 sec...")
            sleep(10)
            print("Resuming...")
            r = session.get(repos_url, params=params)
        r_data = r.json()
        for repo in r_data.get('values'):
            yield repo.get('slug').strip()
        if not r_data.get('next'):
            return
        if page == None:
            page = 1
        page += 1

def add_repo(repo):
    r = session.put(add_url + repo + f"/{workspace}/{group}", data=access)
    while r.status_code == 429:
        print("Hit the API rate limit. Sleeping for 10 sec...")
        sleep(10)
        print("Resuming...")
        r = session.put(add_url + repo + f"/{workspace}/{group}", data=access)
    return r.status_code

def main():
    for repo in get_missing_repos():
        status = add_repo(repo)
        if status == 200:
            print(f"Group {group} was added to the repository {repo} with {access} access")
        else:
            print(status)


if __name__ == '__main__':
    main()